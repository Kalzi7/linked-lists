#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

// The main method has been adjusted to be implemented using a switch.

int main() {
    struct Node *head = NULL;
    int choice, data;

    while (1) {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter data to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 5:
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.

// The implementation of the prototypes is given below.

// Function to create a new node
struct Node *createNode(int num) {
    struct Node *newNode = (struct Node *) malloc(sizeof(struct Node));
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to print the linked list
void printList(struct Node *head) {
    struct Node *temp = head;
    while(temp != NULL) {
        printf("%d ", temp->number);
        temp = temp->next;
    }
    printf("\n");
}

// Function to append a new node to the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if(*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *temp = *head;
    while(temp->next != NULL) 
        temp = temp->next;
    temp->next = newNode;
}

// Function to prepend a new node to the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key) {
    struct Node *temp = *head, *prev = NULL;
    if(temp != NULL && temp->number == key) {
        *head = temp->next;
        free(temp);
        return;
    }
    while(temp != NULL && temp->number != key) {
        prev = temp;
        temp = temp->next;
    }
    if(temp == NULL) return;
    prev->next = temp->next;
    free(temp);
}

// Function to delete a node by value
void deleteByValue(struct Node **head, int value) {
    struct Node *temp = *head, *prev = NULL;
    if(temp != NULL && temp->number == value) {
        *head = temp->next;
        free(temp);
        return;
    }
    while(temp != NULL && temp->number != value) {
        prev = temp;
        temp = temp->next;
    }
    if(temp == NULL) return;
    prev->next = temp->next;
    free(temp); 
}

// Function to insert a node after a given key
void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *temp = *head;
    while(temp != NULL && temp->number != key) 
        temp = temp->next;
    if(temp == NULL) return;
    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Function to insert a node after a given value
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *temp = *head;
    while(temp != NULL && temp->number != searchValue) 
        temp = temp->next;
    if(temp == NULL) return;
    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

